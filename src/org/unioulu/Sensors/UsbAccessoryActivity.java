package org.unioulu.Sensors;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;


public final class UsbAccessoryActivity extends Activity{

	static final String TAG = "UsbAccessoryActivity";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.i(TAG, "LAUNCH");
		Intent i = new Intent();
        i.setClass(this, SensorDataService.class);
		try {
			 startService(i);
		} catch (Exception e) {
			Log.e(TAG, "unable to start sensor activity", e);
		}
		finish();
	}
}
