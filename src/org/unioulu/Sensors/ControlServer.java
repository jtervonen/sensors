package org.unioulu.Sensors;

import java.net.Socket;

import android.content.Context;
import android.content.Intent;

public final class ControlServer extends DataServer
{
	public static final String CONTROL_CLIENTS_CHANGED = "org.unioulu.Sensors.SensorDataService.CONTROL_CLIENTS_CHANGED";
	private static final String sTag = "ControlServer";
	
	//private ReceiverThread mReceiver;
	private boolean RUN = true;
	
	private static class SingletonHolder { 
        public static final ControlServer INSTANCE = new ControlServer();
	}
	public static ControlServer getInstance() {
        return SingletonHolder.INSTANCE;
	}
	
	/*private final class ReceiverThread extends Thread
	{
		public ReceiverThread()
		{
			super();
		}
		
		public void run()
		{
			while (true)
			{
				if (clients.size() == 0)
				{
					try
					{
						sleep(1000);
					}catch (InterruptedException ie){}
				}
				else
					receive();
			}
		}
	}*/
	
	private ControlServer()
	{
		super();
	}
	
	public boolean startOn(String name, int port, Context c)
	{
		if (!super.startOn(name, port, c))
			return false;
		//mReceiver = new ReceiverThread();
		//mReceiver.start();
		return true;
	}
	
	protected void announceClients()
	{
		Intent i = new Intent();
		i.setAction(CONTROL_CLIENTS_CHANGED);
		i.putExtra(EXTRA_NUM_CLIENTS, clients.size());
		mContext.sendBroadcast(i);
	}
	
	protected void handleNewConnection(Socket socket)
	{

		super.handleNewConnection(socket);

		Intent i = new Intent();
		i.setAction(CONTROL_CLIENTS_CHANGED);
		i.putExtra(EXTRA_NUM_CLIENTS, clients.size());
		mContext.sendBroadcast(i);
	}
	
	public boolean hasClients()
	{
		return !clients.isEmpty();
	}
	
	public void stop()
	{
		super.stop();
		/*synchronized(clients)
		{
			for (Socket s : clients)
			{
				try{
					s.close();
				}catch(Exception e){}
			}
			clients.clear();
		}*/
		
		//mReceiver.interrupt();
	}
}