package org.unioulu.Sensors;

import java.net.Socket;

import android.content.Intent;

public class SensorServer extends DataServer {
	
	private static final String sTag = "SensorServer";
	public static final String NETWORK_CLIENTS_CHANGED = "org.unioulu.Sensors.SensorDataService.NETWORK_CLIENTS_CHANGED";
	
	private static class SingletonHolder { 
        public static final SensorServer INSTANCE = new SensorServer();
	}
	public static SensorServer getInstance() {
        return SingletonHolder.INSTANCE;
	}
	
	private SensorServer(){}
	
	protected void announceClients()
	{
		Intent i = new Intent();
		i.setAction(NETWORK_CLIENTS_CHANGED);
		i.putExtra(EXTRA_NUM_CLIENTS, clients.size());
		mContext.sendBroadcast(i);
	}
	
	protected void handleNewConnection(Socket socket)
	{
		super.handleNewConnection(socket);
		
		Intent i = new Intent();
		i.setAction(NETWORK_CLIENTS_CHANGED);
		i.putExtra(EXTRA_NUM_CLIENTS, clients.size());
		mContext.sendBroadcast(i);
	}
}
