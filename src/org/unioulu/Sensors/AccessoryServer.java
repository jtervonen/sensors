package org.unioulu.Sensors;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

import android.content.Context;
import android.content.Intent;
import android.hardware.usb.UsbAccessory;
import android.hardware.usb.UsbManager;

import android.os.ParcelFileDescriptor;
import android.util.Log;

public final class AccessoryServer
{
	private static final String sTag = "AccessoryServer";
	
	public static final String ACCESSORY_CHANNEL_CLOSED = "org.unioulu.Sensors.SensorDataService.ACCESSORY_CHANNEL_CLOSED";
	
	private AccessoryHandler mHandler;
	//private ArrayList<AccessoryHandler> handlers = new ArrayList<AccessoryHandler>(1);
	
	protected UsbManager mUsbManager;
	protected Queue <byte []> mBuffer = new LinkedList<byte[]>();
	protected ParcelFileDescriptor pfd;
	
	protected UsbAccessory accessory;
	protected Context mContext;
	
	private static class SingletonHolder { 
        public static final AccessoryServer INSTANCE = new AccessoryServer();
	}
	public static AccessoryServer getInstance() {
        return SingletonHolder.INSTANCE;
	}
	
	private AccessoryServer(){}
	
	/*public boolean startFor(UsbManager man, UsbAccessory a, Context c)
	{
		mUsbManager = man;
		accessory = a;
		mHandler = new AccessoryHandler("AccessoryHandlerThread");
		mContext = c;

		ParcelFileDescriptor d = mUsbManager.openAccessory(a);
		if (d == null)
		{
			Log.e(sTag,"Accessory fd cannot be opened!");
			return false;
		}
		
		try
		{
			//This should be studied further: Why fd needs to be adopted in order to hold it alive?
			pfd = ParcelFileDescriptor.fromFd(d.detachFd());
		}catch(Exception e)
		{
			Log.e(sTag, "cannot adopt the fd!");
			try {
				d.close();
			} catch (IOException e1) {}
			return false;
		}
		
		stream = new FileOutputStream(pfd.getFileDescriptor());
		
		mHandler.start();
		
		return true;
	}*/
	
	public void stop()
	{
		
		if (mHandler != null)
		{
			Log.i(sTag, "Shutdown Handler!");
			synchronized(mHandler)
			{
				try {
					mHandler.outstream.close();
					mHandler.instream.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			while(mHandler.isAlive()){}
			Log.i(sTag, "Good boy!");
		}
		
		try
		{
			FileOutputStream o = new FileOutputStream(pfd.getFileDescriptor());
			o.close();
			pfd.close();
		}catch(Exception e){e.printStackTrace();}
	}
	
	public boolean startHosting(UsbAccessory a, UsbManager man, Context c)
	{
		stop();
		
		Log.i(sTag, "Stopping the handler!");
		while(isAlive()){}
		Log.i(sTag, "Handler stopped!");
		
		mUsbManager = man;
		mContext = c;
		
		ParcelFileDescriptor d = mUsbManager.openAccessory(a);
		try
		{
			pfd = ParcelFileDescriptor.fromFd(d.detachFd());
		}catch(Exception e)
		{
			Log.e(sTag, "cannot adopt the fd!");
			Intent i = new Intent();
			i.setAction(ACCESSORY_CHANNEL_CLOSED);
			mContext.sendBroadcast(i);
			return false;
		}
		
		FileOutputStream ostream = new FileOutputStream(pfd.getFileDescriptor());
		FileInputStream istream = new FileInputStream(pfd.getFileDescriptor());
		
		mHandler = new AccessoryHandler("AccessoryHandlerThread", ostream, istream);
		mHandler.start();
		return true;
	}
	
	
	private final class AccessoryHandler extends Thread
	{
		private byte [] data;
		private byte [] indata = new byte[32];
		
		protected FileOutputStream outstream;
		protected FileInputStream instream;
		//protected Boolean RUN = Boolean.valueOf(true);
		
		public AccessoryHandler(String name, FileOutputStream o, FileInputStream i)
		{
			super(name);
			outstream = o;
			instream = i;
		}
		
		public void run()
		{
			while (true)
			{	
				synchronized(mBuffer)
				{
					data = mBuffer.poll(); //FIXME: Could make this use Looper and messages instead of polling
				}
				
				if (data != null)
				{
					try
					{
						synchronized(this)
						{
							outstream.write(data);
						}
						data = null;
					}
					catch (IOException ioe)
					{
						Log.e(this.getName(), "ERROR WRITING TO ACCESSORY FD!");
						try
						{
							outstream.close();
							instream.close();
						}catch (Exception e)
						{/*what ever*/}
						Intent i = new Intent();
						i.setAction(ACCESSORY_CHANNEL_CLOSED);
						mContext.sendBroadcast(i);
						break;
					}
				}
			}
		}
	}
	
	public void send(byte [] data)
	{
		synchronized(mBuffer)
		{
			//Log.d(sTag, "AccessoryServer SEND: "+new String(data));
			try
			{
				mBuffer.offer(data);
			}catch(Exception e){
				Log.e(sTag, "ERROR ADDING TO USB BUFFER!");
			}
		}
	}
	
	public boolean isAlive()
	{
		if (mHandler == null)
			return false;
		return mHandler.isAlive();
	}
}