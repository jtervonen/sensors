package org.unioulu.Sensors;

public interface DataListener
{
	public void hear(byte [] data);
}
