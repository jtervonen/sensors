package org.unioulu.Sensors;

import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.PictureCallback;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;


public class SensorActivity extends Activity implements OnSharedPreferenceChangeListener, OnClickListener
{
    private static final String sTag = "SensorActivity";
	
	public static final String CAPTURE_PHOTO = "org.unioulu.Sensors.CAPTURE_PHOTO";
	
	private ScheduledExecutorService scheduleTaskExecutor = Executors.newScheduledThreadPool(1);
	private ScheduledFuture mPichandle;
    private Camera mCamera;
    private SurfaceHolder mSurfaceHolder;
    private boolean mCameraStarted = false;
    
    protected boolean mCameraEnabled = false;
    
    private TextView mInfoView;
    private TextView mInfoView2;
    
    private final BroadcastReceiver mIntentReceiver = new BroadcastReceiver() {
    	
        public void onReceive(Context c, Intent i) {
        	if (i.getAction().equals(ControlServer.CONTROL_CLIENTS_CHANGED))
        	{
    			Bundle b = i.getExtras();
    			if (b != null)
    			{
    				int num = (Integer) b.get(DataServer.EXTRA_NUM_CLIENTS);
    				mInfoView.setText("Control clients:"+Integer.toString(num));
    			}
        	}
        	else if (i.getAction().equals(ImageServer.IMAGE_CLIENTS_CHANGED))
        	{
    			Bundle b = i.getExtras();
    			if (b != null)
    			{
    				int num = (Integer) b.get(DataServer.EXTRA_NUM_CLIENTS);
    				mInfoView2.setText("Image clients:"+Integer.toString(num));
    				
    				if ((Integer) b.get(DataServer.EXTRA_NUM_CLIENTS) == 0)
    					stopCamera();
    				else
    					startCamera();
    			}
        	}
        	else if (i.getAction().equals(AccessoryServer.ACCESSORY_CHANNEL_CLOSED))
        	{
        		Intent in = new Intent();
    			in.setClass(getApplicationContext(), SensorDataService.class);
    			stopService(i);
    			finish();
        	}
        }};
        
	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	Log.i(sTag, "On create");
    	
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.main);
        
        mInfoView = (TextView)findViewById(R.id.infoview);
        mInfoView2 = (TextView)findViewById(R.id.infoview2);
        
        IntentFilter ifilter = new IntentFilter(); 
	    ifilter.addAction(ControlServer.CONTROL_CLIENTS_CHANGED);
	    ifilter.addAction(ImageServer.IMAGE_CLIENTS_CHANGED);
	    ifilter.addAction(AccessoryServer.ACCESSORY_CHANNEL_CLOSED);
	    
	    registerReceiver(mIntentReceiver, ifilter);
	    
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        prefs.registerOnSharedPreferenceChangeListener(this);
        
		if (prefs.getBoolean("enable_service", false))
		{
			Intent i = new Intent();
			i.setClass(this, SensorDataService.class);
			startService(i);
		}
		
		if (prefs.getBoolean("send_images", false))
		{
			mCameraEnabled = true;
			//startCamera();
		}
		
    }
    
    public final PictureCallback onPictureTaken = new PictureCallback() {
    	
    	public void onPictureTaken(byte[] data, Camera camera) {
    		
    		if (data == null)
    			return;
    		
    		BitmapFactory.Options options=new BitmapFactory.Options();
    		options.inSampleSize = 10;
    		Bitmap m = BitmapFactory.decodeByteArray(data,0,data.length,options);
    		
			Intent i = new Intent();
			i.putExtra("image", m);
			i.setAction(ImageServer.PICTURE_TAKEN);
			sendBroadcast(i);
			if (mCamera != null)
				mCamera.startPreview();
		
		}
	
	};
    
    public final SurfaceHolder.Callback onSurfaceCreated = new SurfaceHolder.Callback()
	{
		public void surfaceCreated(SurfaceHolder sh)
		{
			if (mCamera == null)
				return;
			
			try {
				mCamera.setPreviewDisplay(sh);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			mCamera.startPreview();
			
		}

		public void surfaceChanged(SurfaceHolder holder, int format, int width,
				int height) {
			// TODO Auto-generated method stub
			
		}

		public void surfaceDestroyed(SurfaceHolder holder) {
			// TODO Auto-generated method stub
			
		}
		
	};
	private boolean was_paused;
    
	@Override
	protected void onPause()
	{
		Log.i(sTag, "On pause");
		was_paused = true;
		super.onPause();
		unregisterReceiver(mIntentReceiver);
		stopCamera();
	}
	
	@Override
	protected void onResume()
	{
		Log.i(sTag, "On resume");
		super.onResume();
		if (was_paused)
		{
			IntentFilter ifilter = new IntentFilter(); 
		    ifilter.addAction(ControlServer.CONTROL_CLIENTS_CHANGED);
		    ifilter.addAction(ImageServer.IMAGE_CLIENTS_CHANGED);
		    ifilter.addAction(AccessoryServer.ACCESSORY_CHANNEL_CLOSED);
			registerReceiver(mIntentReceiver,ifilter);
		}
		//if (mCameraEnabled && mPichandle == null)
		//	startCamera();
		was_paused = false;
	}
	
	@Override
	protected void onDestroy()
	{
		super.onDestroy();
		stopCamera();
	}
	
	private void stopCamera()
	{
		Log.i(sTag, "Stop camera");
		if (!scheduleTaskExecutor.isShutdown())
		{
			Log.i(sTag, "Not shutdown");
			if (mPichandle != null)
				mPichandle.cancel(true);
			scheduleTaskExecutor.shutdownNow();
			try {
				scheduleTaskExecutor.awaitTermination(1, TimeUnit.SECONDS);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			mPichandle = null;
		}
			
		if (mCamera != null)
		{
			mCamera.stopPreview();
			mCamera.release();
			mCamera = null;
		}
		
		mCameraStarted = false;
	}
	
    
    private void startCamera()
    {
    	Log.i(sTag, "Start camera");
    	
    	if (mCameraStarted)
    		return;
    	
		mCameraStarted = true;
		
		try
		{
			mCamera = Camera.open();
		}catch(Exception e)
		{
			Log.i(sTag, "Exception while getting camera");
			if (mCamera != null)
				Log.i(sTag, "But camera is not null! Bug?");
			e.printStackTrace();
			mCameraStarted = false;
			return;
		}
		if (mCamera == null)
		{
			mCameraStarted = false;
			return;
		}
		
		try {
			mCamera.setDisplayOrientation(90);
			Parameters p = mCamera.getParameters();
			p.setFocusMode(Parameters.FOCUS_MODE_AUTO);
			p.setFlashMode(Parameters.FLASH_MODE_AUTO);
			p.setPictureFormat(ImageFormat.JPEG);
			p.setJpegQuality(50);
			p.setZoom(0);
		} catch (Exception e) {
			Log.i(sTag, "Cannot set camera parameters!");
			e.printStackTrace();
			
		}
		
		if (mSurfaceHolder == null)
		{
			SurfaceView sv = (SurfaceView) findViewById(R.id.previewsurface);
			sv.setOnClickListener(this);
			mSurfaceHolder = sv.getHolder();
			mSurfaceHolder.setSizeFromLayout();
			mSurfaceHolder.addCallback(onSurfaceCreated);
		}
		else
		{
			try {
				mCamera.setPreviewDisplay(mSurfaceHolder);
			} catch (IOException e) {
				Log.i(sTag,"Cannot attach surfaceHolder");
				e.printStackTrace();
				mCameraStarted = false;
				mCamera.release();
				return;
			}
			mCamera.startPreview();
		}	
		startTimer();
    }
    
    private void startTimer()
    {
    	Log.i(sTag, "Start timer");
    	
    	if (mPichandle != null)
    		return;
    	
    	if (scheduleTaskExecutor.isShutdown())
    	{
    		scheduleTaskExecutor = Executors.newScheduledThreadPool(1);
    		Log.i(sTag, "Was shutdown");
    	}
    		
		mPichandle = scheduleTaskExecutor.scheduleAtFixedRate(new Runnable() {
		      public void run() {
		    	Log.i(sTag, "Timer hit! "+Thread.currentThread().getName());
		    	try
		    	{
		    		mCamera.takePicture(null, null, onPictureTaken);
		    	} catch(Exception e)
		    	{
		    		Log.i(sTag, "No Camera!");
		    	}
		      }
			}, 10, 2, TimeUnit.SECONDS);
    }
    
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

    	Intent intent = new Intent();
        intent.setClass(this, SetPreferenceActivity.class);
        startActivityForResult(intent, 0); 
     
        return true;
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.xml.mainmenu, menu);
        return true;
    }

	public void onSharedPreferenceChanged(SharedPreferences prefs, String key)
	{
			
		if(key.equals("enable_service"))
		{
			Log.i(sTag, "Service enable toggled");
			if (!prefs.getBoolean("enable_service", false))
			{
				Intent i = new Intent();
			    i.setClass(this, SensorDataService.class);
			    stopService(i);
			}
			else
			{
				Intent i = new Intent();
			    i.setClass(this, SensorDataService.class);
			    startService(i);
			}
					
		}
		else if(key.equals("send_images"))
		{
			Log.i(sTag, "Send images toggled");
			if (prefs.getBoolean("send_images", false))
			{
				startCamera();
				mCameraEnabled = true;
			}
			else
			{
				stopCamera();
				mCameraEnabled = false;
			}
		}
	}

	public void onClick(View v) {
		// TODO Auto-generated method stub
		
	}	
}
