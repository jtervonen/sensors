package org.unioulu.Sensors;

public final class Helper
{
	
	public static byte[] intToBytes(int i)
	{
		byte [] ret = new byte[2];
		
		ret[0] = (byte) (i  & 0xffff);
		ret[1] = (byte) ((i >>> 8) & 0xff);
		return ret;
	}
	
	public static byte[] longToBytes(long i)
	{
		byte [] ret = new byte[4];
		
		ret[0] = (byte) (i  & 0xffff);
		ret[1] = (byte) ((i >>> 8) & 0xff);
		ret[2] = (byte) ((i >>> 16) & 0xff);
		ret[3] = (byte) ((i >>> 24) & 0xff);
		return ret;
	}
	
	public static int createXOR(byte b [], int len)
	{
		int XOR=0;
		for(int i=0;i<len;i++)
		{
			XOR ^= b[i];
		}
		
		return XOR;
	}
	
	public static byte[] reverse(byte[] data)
	{
	    for (int left = 0, right = data.length - 1; left < right; left++, right--)
	    {
	        byte temp = data[left];
	        data[left]  = data[right];
	        data[right] = temp;
	    }
	    return data;
	}
}
