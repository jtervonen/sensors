/*
 * @Author Jari Tervonen <jjtervonen@gmail.com>
 * @Description Background service for visual navigation aid device
 * @Date 18.06.2012
 * 
 * 
 */

package org.unioulu.Sensors;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Display;
import android.view.Surface;
import android.view.WindowManager;
import android.hardware.usb.*;


public class SensorDataService extends Service implements SensorEventListener, DataListener, LocationListener{

	private static final String sTag = "SensorDataService";
	
	private static final String ACTION_USB_PERMISSION =
		    "org.unioulu.Sensors.USB_PERMISSION";
	
	private static final byte[] SENSOR_PACKET = {(byte)0xff,(byte)0x88,(byte)0xff,(byte)0x88};
	private static final byte[] CONTROL_PACKET = {(byte)0x88,(byte)0xff,(byte)0x88,(byte)0xff,(byte)0x1};
	private static final byte[] STOP_CONTROL = {(byte)0x88,(byte)0xff,(byte)0x88,(byte)0xff,(byte)0x0};
	private static final int PACKET_SIZE = 52;
	private static final int CONTROL_HEADER_SIZE = 5;
	private static final int SENSOR_HEADER_SIZE = 4;
	private static final int CONTROL_PAYLOAD_SIZE = 5;
	
	//private static int SENSOR_DELAY = SensorManager.SENSOR_DELAY_UI;
	private static int SENSOR_DELAY = SensorManager.SENSOR_DELAY_FASTEST;
	private static boolean DEBUG = false;
	
	private SensorManager mSensorManager;
	private WindowManager mWindowManager;
	private LocationManager mLocManager;
	private UsbManager mUsbManager;
	
	private Display mDisplay;
	
	private Sensor mAccelerometer;
	private Sensor mMagneticSensor;
	private Sensor mProximitysensor;

	private int mHeading = 0;
	private double mHeading_raw = 0.0;
	private boolean mNear = false;
	private int mRotated = 0;
	private boolean mTilted = false;
	private Location mLocation = null;
	
	private float[] mAccelValues;
	private float[] mMagneticValues;
	private float linear_acceleration[] = new float [3];
	private float gravity[] = new float [3];
	private final float alpha = 0.8f;
	private final double alpha_h = 0.9;
	
	private float vals [] = new float[3];
	private float[] R = new float[16];
    private float[] I = new float[16];
	
	private SensorServer mDataserver;
	private ControlServer mControlserver;
	private AccessoryServer mAccessoryServer;
	private ImageServer mImageserver;
	
	private byte [] mData = new byte [PACKET_SIZE];
	
	private boolean mRegistered;
	private PendingIntent mPermissionIntent;
	private boolean mPermissionRequestPending = false;
	
	private final BroadcastReceiver mIntentReceiver = new BroadcastReceiver() {
		
		@Override
		public void onReceive(Context c, Intent i) {
			
			if (ACTION_USB_PERMISSION.equals(i.getAction())) {
	            Log.i(sTag, "RECEIVED USB_PERMISSION");
	                UsbAccessory accessory = (UsbAccessory) i.getParcelableExtra(UsbManager.EXTRA_ACCESSORY);
	                if (i.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false))
	                {
	                	if (mPermissionRequestPending)
	                		mPermissionRequestPending = false;
	                	
	                	if(accessory != null){
	                		mAccessoryServer.startHosting(accessory, mUsbManager, getApplicationContext());
	                	}
	                	else
	                	{
	                		startAccessoryServer(accessory);
            				registerListeners();
	                	}
	                }
	                else
	                {
	                    Log.d(sTag, "permission denied for accessory " + accessory);
	                }
	        }
			
			else if (i.getAction().equals(UsbManager.ACTION_USB_ACCESSORY_DETACHED))
			{
				Log.i(sTag, "RECEIVED USB_ACCESSORY_DETACH");
				synchronized (this) {
					stopAccessoryServer();
					Log.i(sTag,"USB was disconnected! Unregistering and shutting down. Bye Bye!");
					stopSelf();
				}
			}
			
			else if (i.getAction().equals(SensorServer.NETWORK_CLIENTS_CHANGED))
			{
				synchronized (this) {
					Bundle b = i.getExtras();
					if (b != null)
					{
						int num = (Integer) b.get(DataServer.EXTRA_NUM_CLIENTS);
						if (num != 0)
							registerListeners();
						else
						{
							if (!mAccessoryServer.isAlive())
								unregisterListeners();
						}
					}
				}
			}
			else if (i.getAction().equals(ControlServer.CONTROL_CLIENTS_CHANGED))
			{
				synchronized (this)
				{
					Bundle b = i.getExtras();
					if (b != null)
					{
						int num = (Integer) b.get(DataServer.EXTRA_NUM_CLIENTS);
						if (num == 0)
						{
							if (mAccessoryServer != null)
							{
								System.arraycopy(STOP_CONTROL, 0, mData, 0, 5);
								mAccessoryServer.send(mData);
							}
						}
					}
				}
			}
			else if (i.getAction().equals(AccessoryServer.ACCESSORY_CHANNEL_CLOSED))
			{
				synchronized (this) {
					Log.i(sTag, "Accessory channel was closed without notice!");
					stopAccessoryServer();
					//stopSelf();
				}
			}
		}
	};
	
	private final OnSharedPreferenceChangeListener mOnSharedListener = new OnSharedPreferenceChangeListener(){

		public void onSharedPreferenceChanged(
				SharedPreferences prefs, String key) {
			if (key.equals("debug"))
				DEBUG = prefs.getBoolean("debug", false);
			else if (key.equals("slow_rate"))
			{
				if (prefs.getBoolean("slow_rate", false))
					SENSOR_DELAY=1000000;
				else
					SENSOR_DELAY=SensorManager.SENSOR_DELAY_FASTEST;
				if (mRegistered)
				{
					unregisterListeners();
					registerListeners();
				}
			}
		}};
	
	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public void onCreate()
	{
		Log.i(sTag, "onCreate");
		mWindowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        mDisplay = mWindowManager.getDefaultDisplay();
        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        mLocManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        mUsbManager = (UsbManager) getSystemService(USB_SERVICE);
        
        mLocation = mLocManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mMagneticSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        mProximitysensor = mSensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
        
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		if (! prefs.getBoolean("enable_service", false))
        	stopSelf();
		
		prefs.registerOnSharedPreferenceChangeListener(mOnSharedListener);
		
        if (prefs.getBoolean("debug", false))
        	DEBUG = true;
        if (prefs.getBoolean("slow_rate", false))
        	SENSOR_DELAY = 1000000;
	    
	    IntentFilter ifilter = new IntentFilter(); 
	    ifilter.addAction(UsbManager.ACTION_USB_ACCESSORY_DETACHED);
	    ifilter.addAction(SensorServer.NETWORK_CLIENTS_CHANGED);
	    ifilter.addAction(ControlServer.CONTROL_CLIENTS_CHANGED);
	    ifilter.addAction(AccessoryServer.ACCESSORY_CHANNEL_CLOSED);
	    ifilter.addAction(ACTION_USB_PERMISSION);
	    
	    registerReceiver(mIntentReceiver, ifilter);
	    
	    mDataserver = ServerFactory.getSensorServer();
	    mDataserver.addListener(this);
	    mDataserver.startOn("NetworkDataServer",9999, this);
	    
	    mControlserver = ServerFactory.getControlServer();
	    mControlserver.addListener(this);
	    mControlserver.startOn("ControlServer",9998,this);
	    
	    mImageserver = ServerFactory.getImageServer();
	    mImageserver.startOn("ImageServer",9997,this);
	    
	    mAccessoryServer = ServerFactory.getAccessoryServer();
	    
	    mPermissionIntent = PendingIntent.getBroadcast(this, 0, new Intent(ACTION_USB_PERMISSION), 0);
	}
	
	public void hear(byte [] data)
	{
		if (DEBUG)
			Log.i(sTag, "Hear: "+data);
		
		if (mAccessoryServer != null)
		{
			//data = Helper.reverse(data);
			System.arraycopy(CONTROL_PACKET, 0, mData, 0, CONTROL_HEADER_SIZE);
			System.arraycopy(data, 0, mData, CONTROL_HEADER_SIZE, CONTROL_PAYLOAD_SIZE);
			int xor = Helper.createXOR(mData, 50);
			mData[51] = (byte)(xor & 0xFF);
			mAccessoryServer.send(mData);
		}
	}
	
	public int onStartCommand(Intent i, int flags, int startId)
	{
	    Log.i(sTag, "STARTING!");
	    
		UsbAccessory[] accessories = mUsbManager.getAccessoryList();
		UsbAccessory accessory = (accessories == null ? null : accessories[0]);
		if (accessory != null) {
			if (mUsbManager.hasPermission(accessory)) {
				Log.i(sTag, "HAS PERMISSION");
				synchronized (this)
				{
					if (!mAccessoryServer.isAlive())
					{
						if (startAccessoryServer(accessory))
							registerListeners();
						else
						{
							Log.e(sTag, "Accessory might be in some strange state - server cannot start!");
							stopSelf();
						}
					}
					else
					{
					Log.d(sTag, "Accessory server should be handling one already?");
					}
				}
			}
			else 
			{
				synchronized (mIntentReceiver)
				{
					if (!mPermissionRequestPending)
					{
						Log.i(sTag, "GET PERMISSION");
						mUsbManager.requestPermission(accessory,mPermissionIntent);
						mPermissionRequestPending = true;
					}
				}
			}
		}
		else
			Log.d(sTag, "accessory is null");

		return START_NOT_STICKY;
	}
	
	public void onDestroy()
	{
		unregisterListeners();
		unregisterReceiver(mIntentReceiver);
		synchronized(this)
		{
			if(mDataserver != null)
			{
				mDataserver.stop();
				mDataserver = null;
			}
			if (mControlserver != null)
			{
				mControlserver.stop();
				mControlserver = null;
			}
			if (mImageserver != null)
			{
				mImageserver.stop();
				mImageserver = null;
			}
		}
	}

	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// TODO Auto-generated method stub
		
	}

	private synchronized void registerListeners()
	{
		Log.i(sTag, "Register listeners");
		if (mRegistered)
			return;
		
		mSensorManager.registerListener(this, mAccelerometer, SENSOR_DELAY);
        mSensorManager.registerListener(this, mProximitysensor, SensorManager.SENSOR_DELAY_FASTEST);
        mSensorManager.registerListener(this, mMagneticSensor, SENSOR_DELAY);
        mLocManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 20*1000, 5, this);
        mRegistered = true;
	}
	
	private synchronized void unregisterListeners()
	{
		Log.i(sTag, "Unregister listeners");
		if (!mRegistered)
			return;
					
		mSensorManager.unregisterListener(this, mAccelerometer);
        mSensorManager.unregisterListener(this, mProximitysensor);
        mSensorManager.unregisterListener(this, mMagneticSensor);
        mLocManager.removeUpdates(this);
        mRegistered = false;
	}
	
	private boolean startAccessoryServer(UsbAccessory a)
	{
		Log.i(sTag, "Start Accessory server");
		synchronized(this)
		{
			if (mAccessoryServer != null)
				return mAccessoryServer.startHosting(a, mUsbManager, getApplicationContext());
			return false;
		}
			
	}
	
	private void stopAccessoryServer()
	{
		Log.i(sTag, "Stop Accessory server");
		synchronized(this)
		{
			if (mAccessoryServer != null)
			{
				System.arraycopy(STOP_CONTROL, 0, mData, 0, 5);
				mAccessoryServer.send(mData);
				mAccessoryServer.stop();
			}
		}
	}
	
	public void onSensorChanged(SensorEvent event)
	{
			
		if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER)
		{
			//float sensorX = 0f;
			//float sensorY = 0f;
			int rot = 0;
			
			mAccelValues = event.values.clone();
			
			switch (mDisplay.getRotation()) {
			case Surface.ROTATION_0:
				//Should always stay like this
				rot=0;
				//sensorX = event.values[0];
				//sensorY = event.values[1];
				break;
			case Surface.ROTATION_90:
				rot=90;
				//sensorX = -event.values[1];
				//sensorY = event.values[0];
				break;
			case Surface.ROTATION_180:
				rot=180;
				//sensorX = -event.values[0];
				//sensorY = -event.values[1];
				break;
			case Surface.ROTATION_270:
				rot=270;
				//sensorX = event.values[1];
				//sensorY = -event.values[0];
				break;
			}

			mRotated = rot;
			
	        gravity[0] = alpha * gravity[0] + (1 - alpha) * event.values[0];
	        gravity[1] = alpha * gravity[1] + (1 - alpha) * event.values[1];
	        gravity[2] = alpha * gravity[2] + (1 - alpha) * event.values[2];

	        if (gravity[1] > 10.0f || gravity[1] < 5.9f) //Check if the device is incorrectly positioned
	        	mTilted = true;
	        else
	        	mTilted = false;
	        
	        linear_acceleration[0] = event.values[0] - gravity[0]; //x
	        linear_acceleration[1] = event.values[1] - gravity[1]; //y
	        linear_acceleration[2] = event.values[2] - gravity[2]; //z
	        
	        if (DEBUG)
	        	Log.i(sTag, linear_acceleration[0]+" "+linear_acceleration[1]+" "+linear_acceleration[2]);
	        
		}
		if (event.sensor.getType() == Sensor.TYPE_PROXIMITY)
		{
			if (DEBUG)
				Log.i(sTag, "Proximity:"+event.values[0]+" "+event.sensor.getResolution()+" "+event.sensor.getMaximumRange());
			
			if (event.values[0] < event.sensor.getMaximumRange())
				mNear=true;
			else
				mNear=false;
		}
		else if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD)
		{
			mMagneticValues = event.values.clone();
		}
		
		if (mMagneticValues != null && mAccelValues != null)
		{
			if (SensorManager.getRotationMatrix(R, I, mAccelValues, mMagneticValues))
			{
				SensorManager.remapCoordinateSystem(R.clone(), SensorManager.AXIS_X, SensorManager.AXIS_Z, R);

				vals = SensorManager.getOrientation(R, vals);
				double degs = 0.0;
				
				if (vals[0] < 0f)
					/* Sensor gives 0-180 [pi - 0] and 180-360 [0 - (-pi)]*/
					degs = Math.toDegrees( 2.0*Math.PI + Float.valueOf(vals[0]).doubleValue());
				else
					degs = Math.toDegrees(vals[0]);
					
				mHeading_raw = (alpha_h * mHeading_raw + (1.0 - alpha_h) * degs);
				
				if (mTilted)
					mHeading = 180;
				else
					mHeading = Double.valueOf(mHeading_raw).intValue();
				
				//Log.i(sTag, new Integer(mHeading).toString());
				if (DEBUG)
					Log.i(sTag, "Orientation:"+vals[0]+" "+vals[1]+" "+vals[2]+" "+mHeading);
			}
		}	
		
		byte [] bytes;
		System.arraycopy(SENSOR_PACKET, 0, mData, 0, SENSOR_HEADER_SIZE);
		mData[4] = mTilted ? (byte)0x1 : (byte)0x0;
		mData[5] = mNear ? (byte)0x1 : (byte)0x0;
		
		bytes = Helper.intToBytes(mHeading);
		System.arraycopy(bytes, 0, mData, 6, 2);
		
		Double theta = Math.atan2(gravity[2], gravity[1]);
		int tilt = (int) (theta*(180.0/Math.PI));
		
		bytes = Helper.intToBytes(tilt);
		System.arraycopy(bytes, 0, mData, 8, 2);
		
		bytes = Helper.longToBytes((long)(linear_acceleration[0]*1000000));
		System.arraycopy(bytes, 0, mData, 10, 4);
		bytes = Helper.longToBytes((long)(linear_acceleration[1]*1000000));
		System.arraycopy(bytes, 0, mData, 14, 4);
		bytes = Helper.longToBytes((long)(linear_acceleration[2]*1000000));
		System.arraycopy(bytes, 0, mData, 18, 4);
		
		int xor = Helper.createXOR(mData, 50);
		mData[51] = (byte)(xor & 0xFF);
		
		/*bytes = Helper.longToBytes((long)(gravity[0]*1000000));
		System.arraycopy(bytes, 0, mData, 20, 4);
		bytes = Helper.longToBytes((long)(gravity[1]*1000000));
		System.arraycopy(bytes, 0, mData, 24, 4);
		bytes = Helper.longToBytes((long)(gravity[2]*1000000));
		System.arraycopy(bytes, 0, mData, 28, 4);*/
		
		//Log.d(sTag, "linear 2:"+Integer.toBinaryString(Float.floatToRawIntBits(linear_acceleration[2])));
		//Log.d(sTag, "bytes[0]:"+Integer.toBinaryString(bytes[0]));
		//Log.d(sTag, "bytes[1]:"+Integer.toBinaryString(bytes[1]));
		//Log.d(sTag, "bytes[2]:"+Integer.toBinaryString(bytes[2]));
		//Log.d(sTag, "bytes[3]:"+Integer.toBinaryString(bytes[3]));
		
		
		if (mDataserver != null)
			mDataserver.send(mData);
			
		if (mAccessoryServer != null)
			mAccessoryServer.send(mData);
	}

	public void onLocationChanged(Location location) {
		mLocation = location;
	}

	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub
		
	}
}
