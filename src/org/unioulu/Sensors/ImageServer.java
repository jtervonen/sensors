package org.unioulu.Sensors;

import java.net.Socket;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.util.Log;

public final class ImageServer extends DataServer
{
	public static final String PICTURE_TAKEN = "org.unioulu.Sensors.SensorDataService.PICTURE_TAKEN";
	public static final String IMAGE_CLIENTS_CHANGED = "org.unioulu.Sensors.SensorDataService.IMAGE_CLIENTS_CHANGED";
	
	private BroadcastReceiver mReceiver;
	private static final String sTag = "ImageServer";
	private IntentFilter mFilter;
	private boolean registered = false;
	
	private static class SingletonHolder { 
        public static final ImageServer INSTANCE = new ImageServer();
	}
	public static ImageServer getInstance() {
        return SingletonHolder.INSTANCE;
	}
	
	private ImageServer(){super();}
	
	@Override
	public boolean startOn(String name, int port, Context c)
	{
		super.startOn(name, port, c);
		
		mReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				//Vector <Socket> toRemove = new Vector<Socket>(1);
				try{
					Log.i(sTag, "Got picture. Sending it now");
		            Bitmap m = intent.getParcelableExtra("image");
					if (m == null)
						return;
					for (ClientHandler h : clients)
					{
						if(!m.compress(Bitmap.CompressFormat.PNG, 100, h.dataout))
						{
							Log.i(sTag, "Cannot write!");
							//nothing
						}
					}	
				}catch (Exception e)
				{
					Log.e(sTag, "Error in writing the image to socket buffer!");
					e.printStackTrace();
				}
			}
		};
		
		mFilter = new IntentFilter();
		mFilter.addAction(PICTURE_TAKEN);
		return true;
	}
	
	protected void announceClients()
	{
		Intent i = new Intent();
		i.setAction(IMAGE_CLIENTS_CHANGED);
		i.putExtra(EXTRA_NUM_CLIENTS, clients.size());
		mContext.sendBroadcast(i);
	}
	
	protected void handleNewConnection(Socket socket)
	{
		super.handleNewConnection(socket);
		if (!registered)
		{
			if (mContext.registerReceiver(mReceiver, mFilter) != null)
				registered = true;
		}
	}
	
	@Override
	public void stop()
	{
		Log.i(sTag, "Stop");
		super.stop();
		if (registered)
		{
			mContext.unregisterReceiver(mReceiver);
			registered = false;
		}
	}
}