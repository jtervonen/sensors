package org.unioulu.Sensors;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Vector;

import android.content.Context;
import android.util.Log;

public abstract class DataServer 
{
	public static final String EXTRA_NUM_CLIENTS = "org.unioulu.Sensors.SensorDataService.EXTRA_NUM_CLIENTS";
	
	
	private static final String sTag = "DataServer";
	
	protected Vector <ClientHandler> clients = new Vector <ClientHandler>(1);
	protected Vector <DataListener> listeners = new Vector <DataListener>(1);
	
	protected ServerSocket serversocket = null;
	private String name;
	private Thread handler;
	protected Context mContext;
	
	public DataServer()
	{
	}
	
	public boolean startOn(String name, int port, Context c)
	{
		this.name = name;
		mContext = c;
		try
		{
			serversocket = new ServerSocket(port);
		}
		catch(IOException ioe){
			Log.e(sTag, "Cannot create serversocket!");
			return false;
		}
				
		handler = new ConnectionHandler(name+":ConnectionHandler");
		handler.start();
		return true;
	}
	
	protected void removeSlave(Thread slave)
	{
		try {
			clients.get(clients.indexOf(slave)).socket.close();
		} catch (IOException e) {}
		
		clients.remove(slave);
		announceClients();
	}
	
	private final class ConnectionHandler extends Thread
	{
		public ConnectionHandler(String name) {
			super(name);
		}

		public void run()
		{
			while (true)
			{
				try
				{
					Socket socket = null;
					socket = serversocket.accept();
					Log.i(sTag+":"+name, "Accepted connection from: "+socket.getInetAddress().toString());
					
					handleNewConnection(socket);
					
				}catch(Exception e){
					e.printStackTrace();
					Log.i(sTag, "Exception while accept() Did you close it?");
					break;
				}
			}
		}
	}
	
	protected void handleNewConnection(Socket socket)
	{
		ClientHandler h = new ClientHandler(this,socket);
		clients.add(h);
		announceClients();
		h.start();
	}
	
	protected final class ClientHandler extends Thread
	{
		public Socket socket;
		protected DataInputStream datain = null;
		protected DataOutputStream dataout = null;
		private DataServer master;
		
		public ClientHandler(DataServer server, Socket s)
		{
			this.socket = s;
			master = server;
			try
			{
				datain = new DataInputStream(socket.getInputStream());
				dataout = new DataOutputStream(socket.getOutputStream());
			}catch(IOException ioe){}
			
		}
		
		public void run()
		{
			while (true)
			{
				if (socket.isClosed())
				{
					notifyClose();
					break;
				}
				receive();
			}
		}
		
		protected void notifyClose()
		{
			master.removeSlave(this);
		}
		
		private void receive()
		{
			synchronized(socket)
			{
				byte buffer[] = new byte[1024];
				try
				{
					int read = datain.read(buffer);
					if (read < 0)
					{
						socket.close();
						return;
					}
				}catch(IOException ioe)
				{
					try {
						socket.close();
					} catch (IOException e) {return;}
				}
				tell(buffer);
			}
		}
		
		public synchronized void send(byte [] data)
		{
			synchronized(socket)
			{
				try {
					dataout.write(data);
				} catch (IOException e) {
					try
					{
						socket.close();
					}catch(IOException ioe)
					{
					}
					notifyClose();
				}
			}
		}
	}
	
	/*protected void handleDisconnects(Vector<Socket>toRemove)
	{
		for (Socket s : toRemove)
		{
			Log.i(sTag, "client dumped us");
			try
			{
				s.close();
			}catch(Exception e){}
			clients.remove(s);
			announceClients();
		}
	}*/
	
	protected void announceClients()
	{
	}
	
	public void send(byte [] data)
	{
		if (clients.isEmpty())
			return;
		
		/*Vector <Socket> toRemove = new Vector<Socket>(1);*/
		
		for (ClientHandler s : clients)
		{
			/*if (s.isInputShutdown())
				toRemove.add(s);*/
				
			/*try
			{
				DataOutputStream o = new DataOutputStream(s.getOutputStream());
				o.write(data);
			}catch(IOException ioe)
			{
				Log.e(sTag,"Error in send. Stream was opened but unable to write!");
			}*/
			s.send(data);
		}
		//handleDisconnects(toRemove);
		
	}
	
	public void addListener(DataListener l)
	{
		listeners.add(l);
	}
	
	
	/*protected void receive()
	{
		if (clients.size() == 0)
			return;
		
		byte [] buffer = new byte[1024];
		
		int datasize = 0;
		Vector <Socket> toRemove = new Vector<Socket>(1);
		
		synchronized(clients)
		{
		for (Socket s : clients)
		{
			if (s.isInputShutdown())
			{
				toRemove.add(s);
				continue;
			}
			
			try
			{
				DataInputStream i = new DataInputStream(s.getInputStream());
				if (i.available()>0)
					datasize = i.read(buffer);
				
				if (datasize == -1)
					toRemove.add(s);
				
			}catch (IOException ioe)
			{
				Log.e(sTag+":"+name, "Error in receive. Stream was opened but unable to write!");
			}
			if (datasize > 0)
				tell(buffer);
		}
		}
		handleDisconnects(toRemove);
		
	}*/
	
	protected synchronized void tell(byte [] buffer)
	{
		for (DataListener l : listeners)
		{
			l.hear(buffer);
		}
	}
	
	public boolean hasClients()
	{
		return !clients.isEmpty();
	}
	
	public boolean isAlive()
	{
		return handler.isAlive();
	}
	
	public void stop()
	{
		try
		{
			for (ClientHandler h : clients)
			{
				try
				{
					h.socket.close();
				}catch(IOException ioe){}
			}
			clients.clear();
			serversocket.close();
		}catch(Exception e){Log.i(sTag, "Server socket close failed");}
	}
}