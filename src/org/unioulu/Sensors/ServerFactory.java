package org.unioulu.Sensors;

public final class ServerFactory{

	private ServerFactory(){}
	
	static ImageServer getImageServer()
	{
		return ImageServer.getInstance();
	}
	static ControlServer getControlServer()
	{
		return ControlServer.getInstance();
	}
	static AccessoryServer getAccessoryServer()
	{
		return AccessoryServer.getInstance();
	}
	static SensorServer getSensorServer()
	{
		return SensorServer.getInstance();
	}
}
